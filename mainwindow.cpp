#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qthugetablemodel.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QElapsedTimer>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{   
    QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL");
    db.setHostName("localhost");
    db.setDatabaseName("huge_db");
    db.setUserName("postgres");
    db.open();
    ui->setupUi(this);
    ui->tableView->setSortingEnabled(true);
    ui->tableView->setModel(new QtHugeTableModel(QStringLiteral("archive_view"),
                                                 QStringList() << "id" << "name_field"));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QElapsedTimer timer;
    timer.start();
    QSqlQuery query(QStringLiteral("insert into archive(name_field) values('%1')").
                    arg(ui->lineEdit->text()));
    qDebug() << query.executedQuery() << timer.elapsed() << "ms";
}

void MainWindow::on_pushButton_2_clicked()
{
    if(QtHugeTableModel *model = dynamic_cast<QtHugeTableModel *>(ui->tableView->model())){
        model->setConditions(ui->lineEdit_2->text());
    }
}
