#include "qthugetablemodel.h"
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlQuery>
#include <QElapsedTimer>
#include <QDebug>

QtHugeTableModel::QtHugeTableModel(QString table,
                                   QStringList columns,
                                   QObject *parent):
    QAbstractItemModel (parent),
    m_table(table),
    m_columns(columns)
{
    QSqlDatabase db = QSqlDatabase::database();
    if(!db.tables().contains(m_table)){
        qDebug() << QString("The table %1 does not exist").arg(table);
    }
    init();
}

QModelIndex QtHugeTableModel::index(int row,
                                    int column,
                                    const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return createIndex(row, column);
}

QModelIndex QtHugeTableModel::parent(const QModelIndex &child) const
{
    Q_UNUSED(child)
    return {};
}

int QtHugeTableModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    QElapsedTimer timer;
    timer.start();
    int count = getValue("count(*)").toInt();
    qDebug() << "int QtHugeTableModel::rowCount(const QModelIndex &parent) const" << timer.elapsed() << "ms";
    return count;
}

int QtHugeTableModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_columns.size();
}

QVariant QtHugeTableModel::data(const QModelIndex &index,
                                int role) const
{
    if(!index.isValid()){
        return {};
    }
    switch (role) {
        case Qt::DisplayRole:
        return getValue(m_columns.at(index.column()), index.row() + 1);
    }
    return {};
}

QVariant QtHugeTableModel::headerData(int section,
                                      Qt::Orientation orientation,
                                      int role) const
{
    if(orientation == Qt::Horizontal){
        switch (role) {
        case Qt::DisplayRole:
                return m_columns.at(section);
        }
    }
    return QAbstractItemModel::headerData(section, orientation, role);
}

void QtHugeTableModel::setConditions(QString condition)
{
    QElapsedTimer timer;
    timer.start();
    emit beginResetModel();
    QSqlQuery query;
    qDebug() << query.exec("DROP MATERIALIZED VIEW archive_view");
    qDebug() << query.exec(
    QStringLiteral("CREATE MATERIALIZED VIEW archive_view AS \
    SELECT row_number() OVER (ORDER BY archive.id) AS row_number, archive.id, archive.name_field \
    FROM archive WHERE %1;").arg(condition));
    qDebug() << query.exec("CREATE INDEX archive_view_idx ON archive_view USING btree \
                            (row_number, id, name_field COLLATE pg_catalog.\"default\")");

    emit endResetModel();

    qDebug() << "archive_view" << timer.elapsed();
}

void QtHugeTableModel::slotNotification(const QString &name)
{
    qDebug() << name;
    if( name == "before_archive_edit" ){
        emit beginResetModel();
    } else if( name == "after_archive_edit" ){
        emit endResetModel();
    }
}

QVariant QtHugeTableModel::getValue(QString columnName,
                                    int rowNumber) const
{
    QString whereStatement(QStringLiteral("row_number=%1").arg(rowNumber));
    if(rowNumber == 0){
        whereStatement = "TRUE";
    }
    QElapsedTimer timer;
    timer.start();
    QSqlQuery query(QStringLiteral("select %1 from %2 where %3").
                    arg(columnName).
                    arg(m_table).
                    arg(whereStatement));
    qDebug() << timer.elapsed() << "ms";
    query.next();
    return query.value(0);
}

void QtHugeTableModel::init()
{
    QSqlDatabase db = QSqlDatabase::database();
    db.driver()->subscribeToNotification("before_archive_edit");
    db.driver()->subscribeToNotification("after_archive_edit");

    connect(db.driver(), static_cast<void (QSqlDriver::*)(const QString &)>(&QSqlDriver::notification),
            this, &QtHugeTableModel::slotNotification, Qt::UniqueConnection);
}
