#ifndef QTHUGETABLEMODEL_H
#define QTHUGETABLEMODEL_H

#include <QAbstractItemModel>

class QtHugeTableModel : public QAbstractItemModel
{
public:
    QtHugeTableModel(QString table, QStringList columns = QStringList(), QObject *parent = nullptr);
    virtual QModelIndex index(int row, int column,
                              const QModelIndex &parent = QModelIndex()) const override;
    virtual QModelIndex parent(const QModelIndex &child) const override;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation,
                                int role = Qt::DisplayRole) const override;

    void setConditions(QString condition);

protected:
    Q_SLOT void slotNotification(const QString &name);
    QString m_table;
    QStringList m_columns;
private:
    QVariant getValue(QString columnName = QStringLiteral("*"), int rowNumber = 0) const;
    void init();

};

#endif // QTHUGETABLEMODEL_H
